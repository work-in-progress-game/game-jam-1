﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intro : MonoBehaviour
{
    public GameObject firsttitle;
    public GameObject secondtitle;
    public GameObject ziemia;
    private void Awake()
    {
        firsttitle.gameObject.SetActive(true);
        StartCoroutine(Example());

        StartCoroutine(Example1());
        StartCoroutine(Example2());
    }
    IEnumerator Example()
    {
        yield return new WaitForSeconds(5);
        
        firsttitle.gameObject.SetActive(false);
        secondtitle.gameObject.SetActive(true);
    }
    IEnumerator Example1()
    {
        yield return new WaitForSeconds(6);
        secondtitle.gameObject.SetActive(false);
        ziemia.GetComponent<Animator>().Play("ziemia"); 
        //Application.LoadLevel(2);
    }
    IEnumerator Example2()
    {
        yield return new WaitForSeconds(8);
        Application.LoadLevel(2);
    }
}
