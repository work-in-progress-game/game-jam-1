﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void Exit() {
        Application.Quit();
    }

    public void LoadScene(string SceneName) {
        SceneManager.LoadScene(SceneName);
    }
}
