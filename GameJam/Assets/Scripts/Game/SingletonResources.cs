﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonResources
{
    private static SingletonResources singleton = new SingletonResources();

    public Ray currentPosition { get; set; }
    public string onClickDecide { get; set; }

    public GameObject chosenElement { get; set; }
    public Defenders defenders { get; set; }

    public List<GameObject> enemies { get; set; }

    public HealthUI health { get; set; }
    public MoneyUI money { get; set; }

    public CanvasUI canvas { get; set; }

    private SingletonResources()
    {
        enemies = new List<GameObject>();
    }

    public static SingletonResources getInstance()
    {
        return singleton;
    }
}
