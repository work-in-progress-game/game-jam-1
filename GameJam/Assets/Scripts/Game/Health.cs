﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : RadiusSize
{
    public float HP = 1f;
    public bool isTurret = false;

    public bool getDamage(float dmg) {
        HP -= dmg;
        if (gameObject.tag == "HQ")
        {
            CanvasUI a = FindObjectOfType<CanvasUI>();
            a.UpdateHealthBar();
        }
        if (HP > 0)
        {
            return false;
        }
        else {
            HP = 0;
            if (gameObject.tag == "Turret")
            {
                FindObjectOfType<Defenders>().iloscTurret--;
                Destroy(gameObject.GetComponentInParent<Turret>().gameObject);
            }
            if (gameObject.tag == "HQ") {
                CanvasUI a = FindObjectOfType<CanvasUI>();
                a.UpdateHealthBar();
                Waves end = FindObjectOfType<Waves>();
                end.EndGame();
            }
            VisualHealth Visual = GetComponent<VisualHealth>();
            if (Visual != null)
            {
                Debug.Log("F1");
                Visual.EffectBurst();
            }
            Behaviour tmpBehaviour = GetComponent<Behaviour>();
            


            bool isRemoved = SingletonResources.getInstance().enemies.Remove(gameObject);

            if(isRemoved)
                SingletonResources.getInstance().money.dodajWartosc(tmpBehaviour.scriptable.cashForKill);

            if (tmpBehaviour != null)
            {
                Waves c = FindObjectOfType<Waves>();
                if (tmpBehaviour.scriptable.combatType == AttackType.Bomber)
                {
                    float ang = 0;
                    for (int i = 0; i < 3; i++)
                    {
                        GameObject temp = c.GenerateAttacker(tmpBehaviour.scriptable.smallBomber, transform.position);
                        temp.transform.position = GeneratorMath.AngleToPosition(ang, 0.75f, temp.transform.position.z, temp.transform.position);
                        ang += 120;
                    }
                }
                c.CheckIfEnd();
            }
            Destroy(gameObject);
            return true;
        }
    }
}
