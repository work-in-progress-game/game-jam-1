﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackType {
    Close, Distance, Bomber
};

[CreateAssetMenu]
public class Attacker : ScriptableObject
{
    public Sprite img;
    public float speed = 1;
    public float damagePerSecond;
    public float howOften = 1;
    public AttackType combatType;
    public float HP = 50;
    public Color mainKolor;
    public float CRadius = 0.2f;
    public AudioClip attackSound;
    public AudioClip dyingSound;
    public AudioClip damageSound;
    public Attacker smallBomber = null;
    public float cashForKill;
}
