﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waves : MonoBehaviour
{

    public int waveID = 0;
    public Fala[] Fale;
    List<float> czasy = new List<float>();
    float czas;
    int num;
    Attacker[] przeciwnicy;
    public Generator generatorPrzeciwnikow;
    public GameObject prefabPrzeciwnika;
    public int currentWave = 0;
    public GameObject przycisk;
    public GameObject sprzedaj;
    public GameObject panelpaneli;
    bool inProgress = false;
    public GameObject GO1, GO2;
    public bool trzepanieHajsu = false;


    public Fala GenerateWave(int difficulty) {
        Fala a = new Fala();
        //jakis pseudo algorytm tu dac
        return a;
    }

    public Fala ReturnFalaID(int id) {
        for (int i = 0; i < Fale.Length; i++) {
            if (Fale[i].idFali == id) {
                return Fale[i];
            }
        }
        return GenerateWave(id);
    }

    public void EndGame() {
        GO1.SetActive(true);
        GO2.SetActive(true);
    }

    void StartWave(int waveID) {
        czasy.Clear();
        Fala a = ReturnFalaID(waveID);
        for (int i = 0; i < a.IloscPrzeciwnikow; i++) {
            czasy.Add(Random.Range(0, a.czasFali));
        }
        przeciwnicy = a.TypyPrzeciwnikow;
        czasy.Sort();
        czas = Time.time;
        num = 0;
        inProgress = true;
        trzepanieHajsu = true;
    }

    public void NextWave() {
        StartWave(currentWave);
        currentWave++;
        przycisk.SetActive(false);
        panelpaneli.SetActive(false);
        sprzedaj.SetActive(false);
        SingletonResources.getInstance().onClickDecide = "";

        //next wave start
    }

    public GameObject GenerateAttacker(Attacker att, Vector3 pos) {
        return generatorPrzeciwnikow.GenerateEnemyAtPosition(prefabPrzeciwnika, att, pos);
    }

    void GenAtt() {
        generatorPrzeciwnikow.GenerateEnemy(prefabPrzeciwnika, przeciwnicy[(int)Random.Range(0, przeciwnicy.Length)]);
    }

    public void CheckIfEnd() {
        if (SingletonResources.getInstance().enemies.Count <= 0 && num >= czasy.Count) {
            przycisk.SetActive(true);
            panelpaneli.SetActive(true);
            sprzedaj.SetActive(true);
            SingletonResources.getInstance().money.dodajWartosc(200f);
            czasy.Clear();
            trzepanieHajsu = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (czasy.Count != 0 && inProgress == true) {
            if (czas + czasy[num] <= Time.time) {
                Debug.Log("Przeciwnik: " + num);
                GenAtt();
                num++;
                if (num >= czasy.Count) {
                    inProgress = false;
                }
            }
        }
    }
}
