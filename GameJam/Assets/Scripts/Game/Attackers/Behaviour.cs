﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class Behaviour : Health
{
    public Attacker scriptable;
    public AttackType type;
    public float speed;
    public Health atakowany;
    public bool atakOn = false;
    float czas;
    float delay;
    LineRenderer linia;
    AudioSource speaker;

    // Start is called before the first frame update
    public void GenStart()
    {
        SpriteRenderer a = GetComponent<SpriteRenderer>();
        a.sprite = scriptable.img;
        CircleCollider2D col = GetComponent<CircleCollider2D>();
        col.radius = scriptable.CRadius;
        transform.Rotate(0, 0, (Mathf.Atan2(transform.position.y, transform.position.x) * Mathf.Rad2Deg) + 90);
        type = scriptable.combatType;
        speed = scriptable.speed;
        delay = scriptable.howOften;
        HP = scriptable.HP;
        speaker = GetComponent<AudioSource>();
        linia = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * Time.deltaTime * speed);
        if (atakOn && czas <= Time.time) {
            Debug.Log("Atak");
            czas += delay;
            if (atakowany == null || atakowany.getDamage(scriptable.damagePerSecond / (1 / scriptable.howOften)))
            {
                speed = scriptable.speed;
                atakOn = false;
                atakowany = null;
            }
            else
            {
                speaker.PlayOneShot(scriptable.attackSound);
                Vector3 dsa1 = new Vector3();
                Vector3 dsa2 = new Vector3();
                dsa1 = transform.position;
                dsa2 = atakowany.transform.position;
                dsa1.z = -1;
                dsa2.z = -1;
                linia.SetPosition(0, dsa1);
                linia.SetPosition(1, dsa2);
                Invoke("ClearLine", 0.3f);
            }
        }
    }

    void ClearLine() {
        linia.SetPosition(0, new Vector3(0, 0, 0));
        linia.SetPosition(1, new Vector3(0, 0, 0));
    }

    void Shoot(GameObject cel) {
        if (scriptable.combatType == AttackType.Close || scriptable.combatType == AttackType.Bomber)
        {
            Debug.Log(cel);
            Debug.Log(cel.tag);
            if (cel.tag == "HQ" || cel.tag == "Turret" || cel.tag == "Defender")
            {
                atakowany = cel.GetComponent<Health>();
                czas = Time.time;
                atakOn = true;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "HQ" || other.gameObject.tag == "Defender" || other.gameObject.tag == "Turret") {
            speed = 0;
            Shoot(other.gameObject);
        }
    }
}
