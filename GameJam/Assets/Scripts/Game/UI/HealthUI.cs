﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthUI : MonoBehaviour
{

    public float value;
    private float maxValue;

    void Start()
    {
        SingletonResources.getInstance().health = this;
        maxValue = value;
    }

    public void zmienWartosc(float value)
    {
        this.value = value;
    }
}
