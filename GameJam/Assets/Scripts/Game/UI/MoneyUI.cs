﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class MoneyUI : MonoBehaviour
{
    public float value;
    public GameObject text;

    void Start()
    {
        SingletonResources.getInstance().money = this;
        text.GetComponent<TextMeshProUGUI>().text = ((int)value).ToString();
    }

    public void zmienWartosc(float value)
    {
        text.GetComponent<TextMeshProUGUI>().text = ((int)value).ToString();
        this.value = value;
    }

    public void aktualizuj()
    {
        text.GetComponent<TextMeshProUGUI>().text = ((int)value).ToString();
    }

    public void dodajWartosc(float value)
    {
        this.value += value;
        text.GetComponent<TextMeshProUGUI>().text = ((int)this.value).ToString();
    }
}
