﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using TMPro;


public class CanvasUI : MonoBehaviour, IPointerDownHandler
{

    public GameObject zycie;
    public GameObject bialko;
    public GameObject[] panele;
    public GameObject panelPaneli;
    public GameObject pojawiajaceSieKoszta;
    /*
     * 
     * Przyciski
     * 
     */
    public GameObject membranaPrzycisk;
    public GameObject munimentumPrzycisk;
    public GameObject ciliaPrzycisk;

    public GameObject cytoplasmaPrzycisk;
    public GameObject mitochondriumPrzycisk;
    public GameObject ribosomaPrzycisk;

    public GameObject primariaPrzycisk;
    public GameObject provectusPrzycisk;
    public GameObject artificjosumPrzycisk;
    public GameObject sprzedajBTN;

    public GameObject przyciskStzalka;

    public Animator hideAndSeek;

    private GameObject[] panelArray0 = new GameObject[3];
    private GameObject[] panelArray1 = new GameObject[3];
    private GameObject[] panelArray2 = new GameObject[3];
    private List<GameObject[]> arrayOfArray = new List<GameObject[]>();

    private const string DEFENDER = "Defender";
    private const string TURRET = "Turret";
    private const string BUILDING = "Building";
    private const string ATACKER = "Atacker";
    private const string HQ = "HQ";
    private const string SPRZEDAJ = "Sprzedaj";


    private bool czyWysuniete = false;
    private Vector3 target = new Vector3(11.5f, 0, 0);
    private float speed = 80;

    public Health HQHealth;
    public Slider hBar;
    private float JHP;
    private float licznik;
    private float licznik2;
    private Color color;
    private bool czyWTrybieSprzedaj = false;

    public void UpdateHealthBar()
    {
        hBar.value = HQHealth.HP / JHP;
    }

    public void uzupelnijiKoszta(string nazwa)
    {
        licznik = 255f;
        pojawiajaceSieKoszta.GetComponent<TextMeshProUGUI>().text = nazwa;
        color = new Color(255, 255, 255, 255);
    }

    public void sprzedawanie(string nazwa)
    {
        licznik2 = 255f;
        pojawiajaceSieKoszta.GetComponent<TextMeshProUGUI>().text = nazwa;
        color = new Color(255, 255, 255, 255);
    }

    int gorny = 0;

    private int[] remeber = new int[3];

    public void SwitchState()
    {
        bool val = hideAndSeek.GetBool("Schowaj");
        if (val == true)
        {
            SingletonResources.getInstance().onClickDecide = "";
            hideAndSeek.SetBool("Schowaj", false);
        }
        else
        {
            hideAndSeek.SetBool("Schowaj", true);
        }
    }

    void Start()
    {

        SingletonResources.getInstance().canvas = this;

        JHP = HQHealth.HP;
        remeber[0] = 0;
        remeber[1] = 1;
        remeber[2] = 2;

        panelArray0[0] = cytoplasmaPrzycisk;
        panelArray0[1] = mitochondriumPrzycisk;
        panelArray0[2] = ribosomaPrzycisk;

        panelArray1[0] = primariaPrzycisk;
        panelArray1[1] = provectusPrzycisk;
        panelArray1[2] = artificjosumPrzycisk;

        panelArray2[0] = membranaPrzycisk;
        panelArray2[1] = munimentumPrzycisk;
        panelArray2[2] = ciliaPrzycisk;

        arrayOfArray.Add(panelArray0);
        arrayOfArray.Add(panelArray1);
        arrayOfArray.Add(panelArray2);

        Button membranaPrzyciskBtn = membranaPrzycisk.GetComponent<Button>();
        Button munimentumPrzyciskBtn = munimentumPrzycisk.GetComponent<Button>();
        Button ciliaPrzyciskBtn = ciliaPrzycisk.GetComponent<Button>();

        Button cytoplasmaPrzyciskBtn = cytoplasmaPrzycisk.GetComponent<Button>();
        Button mitochondriumPrzyciskBtn = mitochondriumPrzycisk.GetComponent<Button>();
        Button ribosomaPrzyciskBtn = ribosomaPrzycisk.GetComponent<Button>();

        Button primariaPrzyciskBtn = primariaPrzycisk.GetComponent<Button>();
        Button provectusPrzyciskBtn = provectusPrzycisk.GetComponent<Button>();
        Button artificjosumPrzyciskBtn = artificjosumPrzycisk.GetComponent<Button>();

        Button przyciskStzalkazBtn = przyciskStzalka.GetComponent<Button>();


        membranaPrzyciskBtn.onClick.AddListener(membrana);
        munimentumPrzyciskBtn.onClick.AddListener(munimentum);
        ciliaPrzyciskBtn.onClick.AddListener(cilia);

        cytoplasmaPrzyciskBtn.onClick.AddListener(cytoplasma);
        mitochondriumPrzyciskBtn.onClick.AddListener(mitochondrium);
        ribosomaPrzyciskBtn.onClick.AddListener(ribosoma);

        primariaPrzyciskBtn.onClick.AddListener(primaria);
        provectusPrzyciskBtn.onClick.AddListener(provectus);
        artificjosumPrzyciskBtn.onClick.AddListener(artificjosum);

        przyciskStzalkazBtn.onClick.AddListener(strzalka);
    }

    /*
     *
     * Akcje przycisków
     * 
     */
    public void membrana()
    {
        SingletonResources.getInstance().onClickDecide = TURRET;
        SingletonResources.getInstance().chosenElement = SingletonResources.getInstance().defenders.Membrania;
    }

    public void munimentum()
    {
        SingletonResources.getInstance().onClickDecide = TURRET;
        SingletonResources.getInstance().chosenElement = SingletonResources.getInstance().defenders.Munimentum;
    }

    public void cilia()
    {
        SingletonResources.getInstance().onClickDecide = TURRET;
        SingletonResources.getInstance().chosenElement = SingletonResources.getInstance().defenders.Cilia;
    }


    public void cytoplasma()
    {
        SingletonResources.getInstance().onClickDecide = "";
        SingletonResources.getInstance().chosenElement = SingletonResources.getInstance().defenders.Cytoplasma;

        float cost = SingletonResources.getInstance().chosenElement.GetComponent<Health>().cost;

        if (SingletonResources.getInstance().money.value < cost)
            return;

        SingletonResources.getInstance().money.value -= cost;
        SingletonResources.getInstance().money.aktualizuj();
        SingletonResources.getInstance().canvas.uzupelnijiKoszta("-" + ((int)cost));

        Defenders a = FindObjectOfType<Defenders>();
        a.turretLimit += 5;
        a.UpdateTurretText();
    }

    public void mitochondrium()
    {
        SingletonResources.getInstance().onClickDecide = BUILDING;
        SingletonResources.getInstance().chosenElement = SingletonResources.getInstance().defenders.Mitochondrium;
    }

    public void ribosoma()
    {
        SingletonResources.getInstance().onClickDecide = BUILDING;
        SingletonResources.getInstance().chosenElement = SingletonResources.getInstance().defenders.Ribosoma;
    }

    public void primaria()
    {
        SingletonResources.getInstance().onClickDecide = DEFENDER;
        SingletonResources.getInstance().chosenElement = SingletonResources.getInstance().defenders.Primaria;
    }

    public void provectus()
    {
        SingletonResources.getInstance().onClickDecide = DEFENDER;
        SingletonResources.getInstance().chosenElement = SingletonResources.getInstance().defenders.Provectus;
    }

    public void artificjosum()
    {
        SingletonResources.getInstance().onClickDecide = DEFENDER;
        SingletonResources.getInstance().chosenElement = SingletonResources.getInstance().defenders.Artificjosum;
    }

    public void sprzedaj()
    {

        if (czyWTrybieSprzedaj)
        {
            sprzedajBTN.GetComponent<Image>().color = Color.white;
            SingletonResources.getInstance().onClickDecide = "";
            czyWTrybieSprzedaj = false;
            SingletonResources.getInstance().defenders.podswietlNaKolor(Color.white);

        }

        else
        {
            sprzedajBTN.GetComponent<Image>().color = Color.grey;
            SingletonResources.getInstance().onClickDecide = SPRZEDAJ;
            czyWTrybieSprzedaj = true;
            SingletonResources.getInstance().defenders.podswietlNaKolor(Color.red);
        }
    }

    public void kliknetyPanel(int numer)
    {
        if (remeber[0] == numer) return;

        zamienPozycjePaneli(panele[remeber[0]], panele[numer], arrayOfArray[remeber[0]], arrayOfArray[numer]);

        if (remeber[1] == numer)
        {
            remeber[1] = remeber[0];
        }
        if (remeber[2] == numer)
        {
            remeber[2] = remeber[0];
        }
        remeber[0] = numer;
    }



    void strzalka()
    {
        speed = 10;

        if (czyWysuniete)
            target = new Vector3(11.5f, 0, 0);
        else
            target = new Vector3(8.4f, 0, 0);

        czyWysuniete = !czyWysuniete;

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        switch (SingletonResources.getInstance().onClickDecide)
        {
            case DEFENDER:
                SingletonResources.getInstance().defenders.createDefender();
                break;
            case TURRET:
                SingletonResources.getInstance().defenders.createTurret();
                break;
            case BUILDING:
                SingletonResources.getInstance().defenders.createBuilding();
                break;
            case SPRZEDAJ:
                SingletonResources.getInstance().defenders.sprzedaj();
                break;
            default:
                break;
        }

    }

    private void zamienPozycjePaneli(GameObject panel1, GameObject panel2, GameObject[] arrayButton1, GameObject[] arrayButton2)
    {
        Vector3 pozycja1 = panel1.gameObject.transform.position;
        Vector3 pozycja2 = panel2.gameObject.transform.position;

        int sortOrder1 = panel1.gameObject.GetComponent<Canvas>().sortingOrder;
        int sortOrder2 = panel2.gameObject.GetComponent<Canvas>().sortingOrder;

        panel1.gameObject.transform.position = pozycja2;
        panel2.gameObject.transform.position = pozycja1;

        panel1.gameObject.GetComponent<Canvas>().sortingOrder = sortOrder2;
        panel2.gameObject.GetComponent<Canvas>().sortingOrder = sortOrder1;

        for (int i = 0; i < 3; i++)
        {
            Vector3 pozycja3 = arrayButton1[i].gameObject.transform.position;
            Vector3 pozycja4 = arrayButton2[i].gameObject.transform.position;

            arrayButton1[i].gameObject.transform.position = pozycja4;
            arrayButton2[i].gameObject.transform.position = pozycja3;

            int sortOrder3 = arrayButton1[i].gameObject.GetComponent<Canvas>().sortingOrder;
            int sortOrder4 = arrayButton2[i].gameObject.GetComponent<Canvas>().sortingOrder;

            arrayButton1[i].gameObject.GetComponent<Canvas>().sortingOrder = sortOrder4;
            arrayButton2[i].gameObject.GetComponent<Canvas>().sortingOrder = sortOrder3;
        }
    }

    private void Update()
    {
        if (licznik > 0)
        {
            licznik--;
            pojawiajaceSieKoszta.GetComponent<TextMeshProUGUI>().color = new Color(255, 0, 0, licznik);
        }

        if (licznik2 > 0)
        {
            licznik2--;
            pojawiajaceSieKoszta.GetComponent<TextMeshProUGUI>().color = new Color(0, 255, 0, licznik2);
        }

    }
}
