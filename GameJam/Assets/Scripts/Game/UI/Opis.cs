﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Opis : MonoBehaviour
{
    public TMPro.TMP_Text opis;
    public GameObject opisGObject;
    public float timeToShow = 5f;

    public void Hide() {
        opisGObject.SetActive(false);
    }

    public void Show() {
        opisGObject.SetActive(true);
    }

    public void ShowOpis(string name, string data) {
        //CancelInvoke("Hide");
        opis.text = "<b><size=12>" + name + "</b></size><br><size=12>" + data + "</size>";
        Show();
        //Invoke("Hide", timeToShow);
    }

    public void OnEnter(opisItems opis) {
        //Debug.Log("nad");
        ShowOpis(opis.name, opis.desc);
    }

    public void OnExit()
    {
        Hide();
        //Debug.Log("opuszczony");
    }
}
