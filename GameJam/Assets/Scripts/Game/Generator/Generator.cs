﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    public GameObject Testowy;
    public Attacker TestowyAttacker;

    public void GenerateEnemy(GameObject enemyPrefab, Attacker enemy) {
        GameObject a = Instantiate(enemyPrefab, GeneratorMath.AngleToPosition(Random.Range(0, 360), 11, 0), Quaternion.Euler(0, 0, 0), this.transform);
        a.GetComponent<Behaviour>().scriptable = enemy;
        a.GetComponent<Behaviour>().GenStart();

        SingletonResources.getInstance().enemies.Add(a);
    }

    public GameObject GenerateEnemyAtPosition(GameObject enemyPrefab, Attacker enemy, Vector3 posToCreate)
    {
        GameObject a = Instantiate(enemyPrefab, posToCreate, Quaternion.Euler(0, 0, 0), this.transform);
        a.GetComponent<Behaviour>().scriptable = enemy;
        a.GetComponent<Behaviour>().GenStart();

        SingletonResources.getInstance().enemies.Add(a);
        return a;
    }
}

static class GeneratorMath{
    static public Vector3 AngleToPosition(float Angle, float Distance, float z)
    {
        Vector3 a = new Vector3(0, 0, z);
        a.x = Distance * Mathf.Cos(Angle);
        a.y = Distance * Mathf.Sin(Angle);
        return a;
    }

    static public Vector3 AngleToPosition(float Angle, float Distance, float z, Vector3 origin)
    {
        Vector3 a = new Vector3(0, 0, z);
        a.x = Distance * Mathf.Cos(Angle) + origin.x;
        a.y = Distance * Mathf.Sin(Angle) + origin.y;
        return a;
    }
}