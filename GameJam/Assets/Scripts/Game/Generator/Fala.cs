﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Fala : ScriptableObject
{
    public int idFali;
    public float czasFali;
    public int IloscPrzeciwnikow;
    public Attacker []TypyPrzeciwnikow;
    public Attacker Boss;
}
