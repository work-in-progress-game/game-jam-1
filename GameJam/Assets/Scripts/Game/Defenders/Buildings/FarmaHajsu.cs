﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarmaHajsu : MonoBehaviour
{
    float iloscHajsu;
    Waves nieMaFal;
    float czas;
    public float coIle = 1;
    public float oIle = 5;

    private void Start()
    {
        nieMaFal = FindObjectOfType<Waves>();
        czas = Time.time;
    }

    void Update()
    {
        if (nieMaFal.trzepanieHajsu && czas < Time.time) {
            iloscHajsu = SingletonResources.getInstance().money.value;
            SingletonResources.getInstance().money.value = iloscHajsu + oIle;
            SingletonResources.getInstance().money.aktualizuj();
            SingletonResources.getInstance().canvas.sprzedawanie("+" + (int)oIle);
            czas += coIle;
        }
    }
}
