﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WincyjWiz : MonoBehaviour
{
    // Start is called before the first frame update
    public void WincyjWiez()
    {
        Defenders a = FindObjectOfType<Defenders>();
        a.turretLimit += 5;
        a.UpdateTurretText();
    }
}
