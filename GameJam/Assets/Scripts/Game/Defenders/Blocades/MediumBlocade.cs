﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MediumBlocade : Blocade
{
    public Vector2 force;
    public float fireRate;
    private float nextFire;

    private void Start()
    {
        nextFire = Time.time;
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Atacker" && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;

            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(force);
        }
    }
}
