﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    public float speed;
    public float damage;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Atacker")
        {
            collision.gameObject.GetComponent<Health>().getDamage(damage);
            Destroy(gameObject);
        }
    }
}
