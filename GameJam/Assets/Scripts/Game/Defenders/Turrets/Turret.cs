﻿using UnityEngine;
using System.Collections.Generic;

public class Turret : Health
{
    public float range;
    public float fireRate;
    public GameObject ammo;
    public GameObject prefab;

    private Vector3 thisPosition;
    private GameObject target;
    private float nextFire;
    public GameObject readyGameObject;

    private void Start()
    {
        thisPosition = gameObject.transform.position;
        thisPosition.z = 10.0f;
        thisPosition = Camera.main.ScreenToWorldPoint(thisPosition);
        readyGameObject = Instantiate(prefab, gameObject.transform.position, Quaternion.Euler(0f, 0f, 0f), this.transform);
    }

    private void Update()
    {
        shoot();
    }

    private void shoot()
    {
        List<GameObject> enemies = new List<GameObject>();
        enemies.AddRange(SingletonResources.getInstance().enemies);
        enemies.Sort(compare);

        if (enemies.Count > 0 && CusMath.distance(enemies[0].transform.position, gameObject.transform.position) < range)
        {
            acquireTargetLock(enemies[0]);
        }
    }

    private void acquireTargetLock(GameObject target)
    {
        Rigidbody2D rb = target.GetComponent<Rigidbody2D>();
        Vector2 targetVelocity = rb.velocity;

        double a = (targetVelocity.x * targetVelocity.x) + (targetVelocity.y * targetVelocity.y) - (ammo.GetComponent<Ammo>().speed * ammo.GetComponent<Ammo>().speed);
        double b = 2 * (targetVelocity.x * (target.gameObject.transform.position.x - gameObject.transform.position.x)
            + targetVelocity.y * (target.gameObject.transform.position.y - gameObject.transform.position.y));
        double c = ((target.gameObject.transform.position.x - gameObject.transform.position.x) * (target.gameObject.transform.position.x - gameObject.transform.position.x)) +
            ((target.gameObject.transform.position.y - gameObject.transform.position.y) * (target.gameObject.transform.position.y - gameObject.transform.position.y));

        double disc = b * b - (4 * a * c);
        if (disc < 0)
        {
            Debug.LogError("No possible hit!");
        }
        else
        {
            double t1 = (-1 * b + Mathf.Sqrt((float)disc)) / (2 * a);
            double t2 = (-1 * b - Mathf.Sqrt((float)disc)) / (2 * a);
            double t = Mathf.Max((float)t1, (float)t2);
            double aimX = (targetVelocity.x * t) + target.gameObject.transform.position.x;
            double aimY = target.gameObject.transform.position.y + (targetVelocity.y * t);

            RotateTo(new Vector3((float)aimX, (float)aimY, 0), 3);

            if (Time.time > nextFire)
            {
                nextFire = Time.time + fireRate;
                sendProjectile(aimX, aimY);
            }
        }
    }

    void RotateTo(Vector3 position, float rotationSpeed)
    {
        Vector3 dir = (position - readyGameObject.gameObject.transform.position).normalized;

        Quaternion newRot = Quaternion.LookRotation(dir, transform.forward);

        readyGameObject.gameObject.transform.rotation = Quaternion.Slerp(readyGameObject.gameObject.transform.rotation, newRot,
        rotationSpeed * Time.deltaTime);

        readyGameObject.gameObject.transform.localEulerAngles = new Vector3(0f, 0f , readyGameObject.gameObject.transform.localEulerAngles.z);
    }

    private void sendProjectile(double aimX, double aimY)
    {
        GameObject gameObjectAmmo = Instantiate(ammo, gameObject.transform.position, Quaternion.Euler(0f, 0f, 0f), this.transform);


        double XminusX0 = aimX - gameObject.transform.position.x;
        double YminusY0 = aimY - gameObject.transform.position.y;
        double sx = (XminusX0 * ammo.GetComponent<Ammo>().speed) / (Mathf.Sqrt((float)(YminusY0 * YminusY0 + XminusX0 * XminusX0)));
        float sy = Mathf.Sqrt((float)(ammo.GetComponent<Ammo>().speed * ammo.GetComponent<Ammo>().speed - sx * sx)) * Mathf.Sign((float)aimY);


        gameObjectAmmo.GetComponent<Rigidbody2D>().velocity = new Vector2((float)sx, (float)sy);
    }

    public int compare(GameObject first, GameObject second)
    {
        var firstPosition = first.transform.position;
        firstPosition.z = 10.0f;
        firstPosition = Camera.main.ScreenToWorldPoint(firstPosition);

        var secondPosition = second.transform.position;
        secondPosition.z = 10.0f;
        secondPosition = Camera.main.ScreenToWorldPoint(secondPosition);

        float firstDistance = CusMath.distance(firstPosition, thisPosition);
        float secondDistance = CusMath.distance(secondPosition, thisPosition);

        return firstDistance > secondDistance ? 1 : -1;
    }
}
