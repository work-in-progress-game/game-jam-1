﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defenders : MonoBehaviour
{
    public GameObject WeakBlocade;

    //Wieżyczki
    public GameObject Membrania;
    public GameObject Munimentum;
    public GameObject Cilia;

    //Budynki
    public GameObject Cytoplasma;
    public GameObject Mitochondrium;
    public GameObject Ribosoma;

    //Mury
    public GameObject Primaria;
    public GameObject Provectus;
    public GameObject Artificjosum;

    private List<GameObject> all;
    private List<GameObject> temporary;

    private int hash;
    private Vector3 clickPosition;
    private int parameter;

    private bool isOn;
    public int turretLimit = 5;
    public int iloscTurret = 0;

    public TMPro.TMP_Text turretText;

    private void Start()
    {
        SingletonResources.getInstance().defenders = this;
        temporary = new List<GameObject>();
        all = new List<GameObject>();
        hash = 0;
        parameter = 3;
        enabled = false;
        isOn = false;
        UpdateTurretText();
    }

    public void UpdateTurretText() {
        turretText.text = (turretLimit - iloscTurret).ToString();
    }

    public void RemovedTurret() {
        iloscTurret--;
        UpdateTurretText();
    }

    private void Update()
    {
        destroyAllTemporary();

        var screenPoint = Input.mousePosition;
        screenPoint.z = 10.0f;
        Vector3 secondPosition = Camera.main.ScreenToWorldPoint(screenPoint);
        List<Point> points = CusMath.line(clickPosition * parameter, secondPosition * parameter, 0.5f);

        float cost = SingletonResources.getInstance().chosenElement.GetComponent<Health>().cost;
        int licznik = (int)(SingletonResources.getInstance().money.value / cost);

        int iterator = 0;
        foreach (Point point in points)
        {
            Collider2D[] colliders = Physics2D.OverlapCircleAll(new Vector3(point.x, point.y, 10f) / parameter, SingletonResources.getInstance().chosenElement.GetComponent<RadiusSize>().radiusSize);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject.tag == "Defender"
                    || colliders[i].gameObject.tag == "Building"
                    || colliders[i].gameObject.tag == "Turret") goto ENDOFLOOPS;
            }


            GameObject gameObject = Instantiate(SingletonResources.getInstance().chosenElement, new Vector3(point.x, point.y, 10f)/parameter, Quaternion.Euler(0, 0, 0), this.transform);
            temporary.Add(gameObject);
            iterator++;

            if (iterator > licznik)
                gameObject.GetComponent<Renderer>().material.color = Color.red;

        }
    ENDOFLOOPS:;
    }

    private void obliczCalkowityKoszt()
    {

    }

    public void podswietlNaKolor(Color color)
    {
        foreach(GameObject iterator in all)
        {
            try
            {
                iterator.GetComponent<Renderer>().material.color = color;
            }
            catch (MissingComponentException e)
            {
                if(iterator != null)
                iterator.GetComponentInParent<Turret>().readyGameObject.GetComponent<Renderer>().material.color = color;
            }
        }
    }

    public void createDefender() {

        if (isOn && Input.GetMouseButtonDown(0))
        {
            float cost = SingletonResources.getInstance().chosenElement.GetComponent<Health>().cost;
            float fullcost = temporary.Count * cost;
            if (SingletonResources.getInstance().money.value < fullcost) return;
            

            SingletonResources.getInstance().money.value -= fullcost;
            SingletonResources.getInstance().money.aktualizuj();
            SingletonResources.getInstance().canvas.uzupelnijiKoszta("-" + ((int)fullcost));

            enabled = false;
            isOn = false;
            all.AddRange(temporary);
            temporary = new List<GameObject>();
        }
        else if (isOn && Input.GetMouseButtonDown(1))
        {
            enabled = false;
            isOn = false;
            destroyAllTemporary();
        }
        else
        {
            enabled = true;
            isOn = true;
            var screenPoint = Input.mousePosition;
            screenPoint.z = 10.0f;
            clickPosition = Camera.main.ScreenToWorldPoint(screenPoint);

            Collider2D[] colliders = Physics2D.OverlapCircleAll(clickPosition, SingletonResources.getInstance().chosenElement.GetComponent<RadiusSize>().radiusSize);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject.tag == "Defender"
                    || colliders[i].gameObject.tag == "Building"
                    || colliders[i].gameObject.tag == "Turret") return;
            }


            GameObject gameObject = Instantiate(SingletonResources.getInstance().chosenElement, clickPosition, Quaternion.Euler(0, 0, 0), this.transform);

            temporary.Add(gameObject);
            hash++;
        }
    }

    public void createTurret()
    {
        if (iloscTurret >= turretLimit) {
            return;
        }
        var screenPoint = Input.mousePosition;
        screenPoint.z = 10.0f;
        clickPosition = Camera.main.ScreenToWorldPoint(screenPoint);


        Collider2D[] colliders = Physics2D.OverlapCircleAll(clickPosition, 0);

        if (!(colliders.Length == 1 && colliders[0].gameObject.tag == "TurretBuildingSite")) return;

        colliders = Physics2D.OverlapCircleAll(clickPosition, SingletonResources.getInstance().chosenElement.GetComponent<RadiusSize>().radiusSize);

        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject.tag == "Defender" 
                || colliders[i].gameObject.tag == "Building" 
                || colliders[i].gameObject.tag == "Turret") return;
        }

        float cost = SingletonResources.getInstance().chosenElement.GetComponent<Health>().cost;

        if (SingletonResources.getInstance().money.value < cost)
            return;

        SingletonResources.getInstance().money.value -= cost;
        SingletonResources.getInstance().money.aktualizuj();
        SingletonResources.getInstance().canvas.uzupelnijiKoszta("-" + ((int)cost));


        GameObject gameObject = Instantiate(SingletonResources.getInstance().chosenElement, clickPosition, Quaternion.Euler(0, 0, 0), this.transform);

        all.Add(gameObject);

        iloscTurret++;
        UpdateTurretText();
    }

    public void sprzedaj()
    {
        var screenPoint = Input.mousePosition;
        screenPoint.z = 10.0f;
        clickPosition = Camera.main.ScreenToWorldPoint(screenPoint);
        Collider2D[] colliders = Physics2D.OverlapCircleAll(clickPosition, 0);

        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject.tag == "Defender"
                || colliders[i].gameObject.tag == "Building"
                || colliders[i].gameObject.tag == "Turret")
            {
                SingletonResources.getInstance().money.value += (colliders[i].gameObject.GetComponent<Health>().cost / 3)*2;
                SingletonResources.getInstance().money.aktualizuj();
                SingletonResources.getInstance().canvas.sprzedawanie("+" +((int)(colliders[i].gameObject.GetComponent<Health>().cost/3)*2).ToString());
                iloscTurret--;
                UpdateTurretText();
                Destroy(colliders[i].gameObject);
            }
        }
    }

    public void createBuilding()
    {
        var screenPoint = Input.mousePosition;
        screenPoint.z = 10.0f;
        clickPosition = Camera.main.ScreenToWorldPoint(screenPoint);

        Collider2D[] colliders = Physics2D.OverlapCircleAll(clickPosition, 0);

        if (!(colliders.Length == 1 && colliders[0].gameObject.tag == "BuildingSite")) return;

        colliders = Physics2D.OverlapCircleAll(clickPosition, SingletonResources.getInstance().chosenElement.GetComponent<RadiusSize>().radiusSize);

        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject.tag == "Defender"
                || colliders[i].gameObject.tag == "Building"
                || colliders[i].gameObject.tag == "Turret") return;
        }

        float cost = SingletonResources.getInstance().chosenElement.GetComponent<RadiusSize>().cost;

        if (SingletonResources.getInstance().money.value < cost)
            return;

        SingletonResources.getInstance().money.value -= cost;
        SingletonResources.getInstance().money.aktualizuj();
        SingletonResources.getInstance().canvas.uzupelnijiKoszta("-" + ((int)cost));

        GameObject gameObject = Instantiate(SingletonResources.getInstance().chosenElement, clickPosition, Quaternion.Euler(0, 0, 0), this.transform);

        all.Add(gameObject);
    }

    private void destroyAllTemporary()
    {
        foreach (GameObject gameObject in temporary)
        {
            Destroy(gameObject);
        }
        temporary = new List<GameObject>();
    }

}
