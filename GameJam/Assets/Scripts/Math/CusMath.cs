﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CusMath 
{
    public static List<Point> line(Vector3 p0, Vector3 p1, float granularity)
    {
        List <Point> points = new List<Point>();
        float dx = p1.x - p0.x;
        float dy = p1.y - p0.y;
        float N = Mathf.Max(Mathf.Abs(dx), Mathf.Abs(dy));
        float divN = (N == 0) ? 0.0f : 1.0f / N;
        float xstep = dx * divN;
        float ystep = dy * divN;
        float x = p0.x, y = p0.y;
        for (float step = 0; step <= N; step++, x += xstep, y += ystep)
        {
            points.Add(new Point(Mathf.Round(x), Mathf.Round(y)));
        }
        return points;
    }

    public static float distance(Vector3 first, Vector3 second) {
        return Vector3.Distance(first, second);
    }

    public static float Distance(Vector3 a, Vector3 b) {
        float result;
        result = Mathf.Sqrt(Mathf.Pow((a.x - b.x), 2) + Mathf.Pow((a.y - b.y), 2));
        return result;
    }
}
public class Point
{
    public float x { get; set; }
    public float y { get; set; }

    public Point(float x, float y)
    {
        this.x = x;
        this.y = y;
    }
}