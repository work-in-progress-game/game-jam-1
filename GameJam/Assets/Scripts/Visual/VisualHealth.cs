﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualHealth : MonoBehaviour
{
    public GameObject psystem;
    public Color kolor;
    ParticleSystem particles;
    ParticleSystem.EmissionModule part;
    bool czyBeh = false;
    AudioSource secret;
    public AudioClip dieSound;
    public bool die = false;
//    GameObject tmp;

    private void Start()
    {
        particles = psystem.GetComponent<ParticleSystem>();
        Attacker b = GetComponent<Behaviour>().scriptable;
        if (b != null) {
            kolor = b.mainKolor;
            czyBeh = true;
        }
    }

    public void EffectBurst() {
        Debug.Log("F2");
        ParticleSystem.MainModule a = particles.main;
        a.startColor = kolor;
        Vector3 tPos = transform.position;
        tPos.z = -1;
        Debug.Log("Czy behaviour Umieranie: " + czyBeh);
        GameObject obiekt = Instantiate(psystem, tPos, transform.rotation);
        if (czyBeh) {
            dieSound = GetComponent<Behaviour>().scriptable.dyingSound;
        }
        if (die) {
            secret = obiekt.GetComponent<AudioSource>();
            Debug.Log(secret);
            secret.PlayOneShot(dieSound);
        }
        /*particles = tmp.GetComponent<ParticleSystem>();
        /*part = particles.emission;
        Invoke("StopPlaying", 0.5f);
        Invoke("EndOfFun", 2.5f);
        part.enabled = true;*/
    }

    /*public void StopPlaying() {
        part.enabled = false;
    }

    public void EndOfFun() {
        Destroy(tmp);
    }*/
}
