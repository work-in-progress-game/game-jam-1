﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackEffect : MonoBehaviour
{
    public bool czyAktywna = false;
    public Vector3 from = new Vector3();
    public Vector3 to = new Vector3();
    public ParticleSystem effect;


    public void AnimationOn(Vector3 a, Vector3 b) {
        if (a == b) {
            return;
        }
        ParticleSystem.MainModule c = effect.main;
        float particleSpeed = 0;
        particleSpeed = effect.main.startSpeedMultiplier;
        Debug.Log("P Speed: " + particleSpeed);
        from = a;
        to = b;
        czyAktywna = true;
        float lifeTime = effect.main.startLifetimeMultiplier;
        c.startLifetimeMultiplier = CusMath.Distance(a, b) / particleSpeed;
        Debug.Log("P Life: " + lifeTime);
        //effect.shape.rotation.Set(0, (Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg) + 90, 0);
        
        effect.gameObject.transform.Rotate(0, 0, (Mathf.Atan2(b.y - a.y, b.x - a.x) * Mathf.Rad2Deg) % 360, Space.World);
        
        Debug.Log((effect.gameObject.transform.rotation.z * Mathf.Rad2Deg) % 360);
        
        c.startRotationZ = (effect.gameObject.transform.rotation.z * Mathf.Rad2Deg) % 360;
    }

    private void Start()
    {
        AnimationOn(new Vector3(0, 0, 0), new Vector3(-5, 10, 0));
    }

    void Update()
    {
        if (czyAktywna) {

        }
    }
}
