﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour
{
    public Color kolor;
    ParticleSystem particles;
    ParticleSystem.EmissionModule part;

    private void Start()
    {
        particles = GetComponent<ParticleSystem>();
        particles = GetComponent<ParticleSystem>();
        part = particles.emission;
        Invoke("StopPlaying", 0.5f);
        Invoke("EndOfFun", 2.5f);
        part.enabled = true;
    }

    public void StopPlaying() {
        part.enabled = false;
    }

    public void EndOfFun() {
        Destroy(this.gameObject);
    }
}
